#define WOKWI
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/float.h"     
#include "pico/double.h"   



float singlePrecision(float precision){                              // calculate the number in single precision 
     float p, pi = 2;                                                  //initialize the variables in the wallis product
   for(int i = 2; i <= precision; i+=2) {                            //loop through a 100000 times and get a pi using wallis product
      p = i;
      pi = pi * ((p) / (i - 1)) * (p / (i + 1));  
            
    }                                                               
    return pi;
}

double doublePrecision(double precision) {                         //calculate the number in double precision method
    double p, pi = 2.0;                                                  //initialize the variables in the wallis product
   for(int i = 2; i <= precision ; i += 2) {                      //loop through a 100000 times and get a pi using wallis product
        p = i;
        pi = pi * ((p) / (i - 1)) * (p / (i + 1));               
    }                                                                
    return pi;
}

int main(){

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    float pi1 = singlePrecision(100000);
    printf("Single precision PI is: %.10f\n",pi1);                                        //get to the first 11 digit of numbers
    float singleApproximationError = pi1 - 3.14159265359;                                //calculate the ApproximationError error
    if(singleApproximationError < 0) 
    singleApproximationError = -singleApproximationError;                               //change to positive value
    printf("Single Approximation error is : %.10f\n",singleApproximationError);

    double pi2 = doublePrecision(100000);
    printf("Double Approximation PI is: %.10f\n",pi2);                                  //get to the first 11 digit of numbers
    double doubleApproximationError = pi2 - 3.14159265359;                              //calculate the ApproximationError error
    if(doubleApproximationError < 0) 
    doubleApproximationError = -doubleApproximationError;                              //change to positive value
    printf("Double Approximation error is : %.10f\n",doubleApproximationError);

  return(0);
}
