#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.align  4

.equ    DFLT_STATE_STRT, 1            @ Specify the value to start flashing
.equ    DFLT_STATE_STOP, 0            @ Specify the value to stop flashing
.equ    DFLT_ALARM_TIME, 1000000      @ Specify the default alarm timeout

.equ    GPIO_BTN_DN_MSK, 0x00040000   @ Bit-18 for falling-edge event on GP20
.equ    GPIO_BTN_EN_MSK, 0x00400000   @ Bit-22 for falling-edge event on GP21
.equ    GPIO_BTN_UP_MSK, 0x04000000   @ Bit-26 for falling-edge event on GP22

.equ    GPIO_BTN_DN,  20              @ Specify pin for the "down" button
.equ    GPIO_BTN_EN,  21              @ Specify pin for the "enter" button
.equ    GPIO_BTN_UP,  22              @ Specify pin for the "up" button
.equ    GPIO_LED_PIN, 25              @ Specify pin for the built-in LED
.equ    GPIO_DIR_IN,   0              @ Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,  1              @ Specify output direction for a GPIO pin

.equ    LED_VAL_ON,    1              @ Specify value that turns the LED "on"
.equ    LED_VAL_OFF,   0              @ Specify value that turns the LED "off"

.equ    GPIO_ISR_OFFSET, 0x74         @ GPIO is int #13 (vector table entry 29)
.equ    ALRM_ISR_OFFSET, 0x40         @ ALARM0 is int #0 (vector table entry 16)

@ Entry point to the ASM portion of the program
main_asm:
    ldr   r3, =LED_VAL_ON               
    ldr   r4, =LED_VAL_OFF    
    ldr   r5, =DFLT_ALARM_TIME       
    ldr   r6, =DFLT_STATE_STRT 
    bl    init_leds                    @ Initialise the GPIO LED pin
    bl    init_buttons                 @ initalize buttons
    bl    install_alrm_isr     
    bl    install_gpio_isr     
    
loop:
    bl    set_alarm                   
    wfi                     
    b     loop                 

init_leds:
    push    {lr}                        @ Store the link register to the stack
    movs    r0, #GPIO_LED_PIN           @ This value is the GPIO LED pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_LED_PIN           @ We want this GPIO pin to be setup as an output pin
    movs    r1, #GPIO_DIR_OUT           @ We want this GPIO pin to be setup as an output pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1
    pop     {pc}

init_buttons:
    push    {lr}                        @ Store the link register to the stack  


    movs    r0, #GPIO_BTN_DN            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_BTN_DN            @ This value is the GPIO button on the PI PICO board
    movs    r1, #GPIO_DIR_IN            @ This is the value of input direction for a GPIO pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO button specified by r0 to state specified by r1
    movs    r0, #GPIO_BTN_DN            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_set_irq            @ Call the subroutine to set the GPIO GPIO interrupt controller by r0 to state specified by r1

    movs    r0, #GPIO_BTN_EN            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_BTN_EN            @ This value is the GPIO button on the PI PICO board
    movs    r1, #GPIO_DIR_IN            @ This is the value of input direction for a GPIO pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO button specified by r0 to state specified by r1           
    movs    r0, #GPIO_BTN_EN            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_set_irq            @ Call the subroutine to set the GPIO GPIO interrupt controller by r0 to state specified by r1


    movs    r0, #GPIO_BTN_UP            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_BTN_UP            @ This value is the GPIO button on the PI PICO board
    movs    r1, #GPIO_DIR_IN            @ This is the value of input direction for a GPIO pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO button specified by r0 to state specified by r1    
    movs    r0, #GPIO_BTN_UP            @ This value is the GPIO button on the PI PICO board
    bl      asm_gpio_set_irq            @ Call the subroutine to set the GPIO GPIO interrupt controller by r0 to state specified by r1       
   
    pop     {pc}

set_alarm:
    movs    r1, #1
    ldr     r2, =(TIMER_BASE + TIMER_INTE_OFFSET)    @ enable the timer interrupts
    str     r1, [r2]   
    ldr     r1, =(TIMER_BASE + TIMER_TIMELR_OFFSET)  
    ldr     r2,[r1]                                 
    movs    r1, r5                                  
    add     r1, r1, r2                               @ add the time delay to timer 
    ldr     r2, =(TIMER_BASE + TIMER_ALARM0_OFFSET)  
    str     r1, [r2]                                 @ store new delay to alarm address
    bx      lr                                      

install_alrm_isr:
    ldr     r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)        @ copy the address of the new ISR to the appropriate entry in the vector table (PPB_BASE + M0PLUS_VTOR_OFFSET + ALARM_ISR_OFFSET).
    ldr     r1, [r2]                                
    ldr     r2, =ALRM_ISR_OFFSET                        
    adds    r2, r1                                      @ add the offset to r1 and store in r2 
    ldr     r0, =alrm_isr                               @ load r0 with the address of return from the alrm_isr 
    str     r0, [r2]                                   
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)   @ Disable the specific interrupt level by writing to the appropriate bit in (PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)
    movs    r0, #1
    str     r0, [r2]
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)    @ Enable the specific interrupt level by writing to the appropriate bit in (PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)
    movs    r0, #1
    str     r0, [r2]
    bx      lr                                       

.thumb_func
alrm_isr:
  
   push    {lr}
   ldr     r1, =(TIMER_BASE+TIMER_INTR_OFFSET)         
   movs    r0 ,#1                                      
   str     r0,[r1]                                       
   movs    r0, #GPIO_LED_PIN                             
   bl     asm_gpio_get                                   @ check the state of the GPIO LED PIN
   cmp     r0, #LED_VAL_OFF                              @ if GPIO LED PIN is off then turn it on
   beq     led_on                                    
led_off:
    movs     r1, #LED_VAL_OFF                            @ turn off led if it is on
    b         led_state                              
led_on:
   movs    r1, #LED_VAL_ON                               @ turn off led if it is on
led_state:
    movs    r0, #GPIO_LED_PIN                            @ load register with led GPIO pin number 
    bl      asm_gpio_put                                 @ get current value of the LED GPIO pin 
    bl      set_alarm                                     @ reset alarm
    ldr     r2, =(TIMER_BASE + TIMER_INTE_OFFSET)         @ Disable pending interrupt
    movs    r1,#1                                        
    str     r1, [r2]                                      @ enable alarm
   
    pop     {pc}                        

install_gpio_isr:
    ldr     r1, =(PPB_BASE + M0PLUS_VTOR_OFFSET)          @ Add the address of the handler entry point to the RAM vector table 
    ldr     r0, [r1]                                      @ copy the address of r1 to r0
    ldr     r2, =GPIO_ISR_OFFSET  
    adds    r0, r0, r2
    ldr     r1, =gpio_isr               
    str     r1, [r0]                                      @ store the address of the gpio_isr subroutine in the vector table
    
    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)
    movs    r0, #1
    ldr     r0, =0x2000                                   @ disable the GPIO IRQ 
    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)                         
    str     r0, [r1]                                      @ enable the GPIO IRQ 
    bx      lr                                        

.thumb_func
gpio_isr:
    push    {lr}
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET)
    ldr     r0, [r2]                                      @ get the interrupt state
    ldr     r1, =GPIO_BTN_DN_MSK
    ands    r0, r0, r1
    CMP     r0, r1                                        @ compare to GPIO_BTN_DN_MSK
    BEQ     down_button

    ldr     r0, [r2]                                     
    ldr     r1, =GPIO_BTN_EN_MSK
    ands    r0, r0, r1
    CMP     r0, r1                                        @ compare to GPIO_BTN_EN_MSK
    BEQ     en_button

    ldr     r0, [r2]       
    ldr     r1, =GPIO_BTN_UP_MSK      
    ands    r0, r0, r1
    CMP     r0, r1                                        @ compare result to GPIO_BTN_UP_MSK
    BEQ     up_button
    b       reset_alarm

reset_alarm:
    push    {r0}
    ldr     r0, =msg4
    bl      printf
    pop     {r0}
    ldr     r5, =DFLT_ALARM_TIME

finish:
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)
    str     r0, [r2]                                     
    pop     {pc}                                     

 @ when the button GP20 is pressed, the flashing interval is halved and a message is printed
down_button: 
    cmp     r6, #1
    beq     halved_flash
    ldr     r0, =GPIO_BTN_DN_MSK
    b       reset_alarm
halved_flash:
    ldr     r0, =msg3
    bl      printf
    lsls    r5, r5, #1 
    ldr     r0, =GPIO_BTN_DN_MSK
    b      finish

@ when the button GP21 is pressed, the flashing interval is reset and a message is printed
en_button:
    cmp     r6, #1              
    beq     reset         
    ldr     r0, =msg1
    bl      printf
    ldr     r0, =GPIO_BTN_EN_MSK
    movs    r6, #DFLT_STATE_STRT 
    b      finish
reset:
    ldr     r0, =GPIO_BTN_EN_MSK
    movs    r6, #0               
    b      finish 

 @ when the button GP22 is pressed, the flashing interval is doubled and a message is printed
up_button:
    cmp     r6, #1
    beq     doubled_flash
    ldr     r0, =GPIO_BTN_UP_MSK
    b       reset_alarm
doubled_flash:
    ldr     r0, =msg2
    bl      printf
    lsrs    r5, r5, #1    
    ldr     r0, =GPIO_BTN_UP_MSK
    b      finish



.align 4
msg1:    .asciz "LED flashing.\n"
msg2:    .asciz "LED flashing doubled.\n"
msg3:    .asciz "LED flashing halved.\n"
msg4:    .asciz "reset flash rate.\n"

.data
lstate: .word   DFLT_STATE_STRT
ltimer: .word   DFLT_ALARM_TIME
